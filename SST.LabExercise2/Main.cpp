#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


// Create a data structure to call enumerators
struct Card
{
	Rank number;
	Suit suit;
};

// Enumerator rank 2 - 14
enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

// Enumerator for four suits
enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES
};

int main()
{




	_getch();
	return 0;
}